const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const dataCollectSchema = new Schema({

   clientId:{
       type:String,
       required:true
   },
   courses:[
       {
       
        programname:{
          type:String,
          required:true
        },
        eligibility:{
          type:String,
          required:true
        },
        durationinyear:{
          type:Number,
          required:true
        },
        
        scope:{
          type:String,
          required:true
        },
        careers:{
          type:String,
          required:true
        }
       
       }
   ]

    ,
  
  date:{
    type:Date,
    default:Date.now
  }},{
  timestamps: true,

});

const DataCollect = mongoose.model('DataCollect',dataCollectSchema);

module.exports = DataCollect;
